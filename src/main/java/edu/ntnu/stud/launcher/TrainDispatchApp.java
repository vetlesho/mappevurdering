package edu.ntnu.stud.launcher;

import edu.ntnu.stud.view.UserInterface;

/**
 * This is the main class for the train dispatch application.
 *
 * <p>Creates an instance of UserInterface-class,
 * and calls init() and start().
 *
 * @author 10026
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {
  public static void main(String[] args) {
    UserInterface trainDispatchUi = new UserInterface();

    trainDispatchUi.init();
    trainDispatchUi.start();
  }
}
