package edu.ntnu.stud.view;

import java.util.Scanner;

/**
 * Utility class for handling user input.
 * Assists with maintaining an intuitive and error-free interaction with the user.
 *
 * @author 10026
 * @version 1.0
 * @since 0.5
 */
public class UserInputHandler {
  private final Scanner scanner = new Scanner(System.in);

  /**
   * Reads and retrieves a string input in the format "hh:mm" from the user.
   *
   * @return The string input provided by the user, representing a time in "hh:mm" format.
   */
  public String readStringForLocalTime() {
    String userInput = "";
    boolean validInput = false;

    while (!validInput) {
      userInput = scanner.nextLine().trim();
      //Used ChatGPT to generate the regular expression.
      //Regex checks if the String is written with numbers and ':'.
      if (!userInput.matches("\\d{2}:\\d{2}")) {
        System.out.println("Invalid input. Please enter a time in the format hh:mm.");
      } else {
        validInput = true;
      }
    }

    return userInput;
  }

  /**
   * Reads and validates user input for Line, allowing a maximum of six characters.
   *
   * @return Validated user input for Line.
   */
  public String readLine() {
    String userInput = "";
    boolean validInput = false;

    while (!validInput) {
      userInput = scanner.nextLine().trim();
      if (userInput.length() > 6 || userInput.contains(" ") || userInput.isBlank()) {
        System.out.println("Invalid input. Please enter a line of 1-6 characters.");
      } else {
        validInput = true;
      }
    }

    return userInput;
  }

  /**
   * Reads and validates user input for a positive integer.
   *
   * @return Validated positive integer entered by the user.
   */
  public int readPositiveInt() {
    int userInput = 0;
    boolean validInput = false;

    while (!validInput) {
      if (scanner.hasNextInt()) {
        userInput = scanner.nextInt();
        if (userInput > 0) {
          scanner.nextLine();
          validInput = true;
        } else {
          System.out.println("Invalid input. Please enter a positive integer.");
          scanner.nextLine();
        }
      } else {
        System.out.println("Invalid input. Please enter a positive integer.");
        scanner.nextLine();
      }
    }
    return userInput;
  }

  /**
   * Reads and validates user input for a destination consisting of letters only.
   *
   * @return Validated destination input entered by the user.
   */
  public String readDestination() {
    String userInput = "";
    boolean validInput = false;

    while (!validInput) {
      userInput = scanner.nextLine().trim();
      //Used ChatGPT to generate the regular expression.
      //Regex checks if the String only consists of norwegian letters.
      if (!userInput.matches("[A-Za-zæøåÆØÅ\\s]+") || userInput.isBlank()) {
        System.out.println("Invalid input. Please enter a destination of only letters.");
      } else if (userInput.length() > 20) {
        System.out.println("Invalid input. Destination can max be 20 characters long.");
      } else {
        validInput = true;
      }
    }

    return userInput;
  }


  /**
   * Reads and validates user input for a track number between 1 and 6 or an empty input.
   *
   * @return Validated track input entered by the user (empty string or a number between 1 and 6).
   */
  public String readTrackString() {
    String trackInput = "";
    boolean validInput = false;

    while (!validInput) {
      trackInput = scanner.nextLine().trim();
      if (trackInput.isBlank() || (trackInput.matches("[0-6]") && trackInput.length() == 1)) {
        validInput = true;
      } else {
        System.out.println("Invalid input. Please enter a number between 1 and 6 or press enter.");
      }
    }
    return trackInput;
  }
}
