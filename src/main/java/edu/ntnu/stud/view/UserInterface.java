package edu.ntnu.stud.view;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainStation;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Manages user interactions for train departure operations.
 * The class provides methods to add or modify train departures,
 * and update the system clock.
 *
 * @author 10026
 * @version 1.0
 * @since 0.1
 */
public class UserInterface {
  private UserInputHandler userInput;
  private TrainStation overview;
  private static final int SHOW_TRAIN_DEPARTURE = 1;
  private static final int ADD_TRAIN_DEPARTURE = 2;
  private static final int ASSIGN_TRACK = 3;
  private static final int ADD_DELAY = 4;
  private static final int SEARCH_DEPARTURE_BY_TRAIN_NUMBER = 5;
  private static final int SEARCH_DEPARTURES_BY_DESTINATION = 6;
  private static final int CHANGE_SYSTEM_CLOCK = 7;
  private static final int END_PROGRAM = 9;
  boolean quit;

  /**
   * init method to initialize overview-class and userInput-class.
   */
  public void init() {
    this.userInput = new UserInputHandler();
    this.overview = new TrainStation();
  }

  /**
   * Start-method to the system.
   *
   * <p>Consists of a switch-case with every method for the system.
   */
  public void start() {
    overview.addTrainDeparture("22:00", "V4", 35, "Bergen", "2");
    overview.addTrainDeparture("15:00", "V4", 36, "Bergen", "2");
    overview.addTrainDeparture("15:00", "N3", 37, "Trondheim", "4");
    overview.addTrainDeparture("13:00", "F5", 24, "Gardemoen", "3");
    overview.addTrainDeparture("19:00", "F5", 26, "Gardemoen", "3");

    System.out.println("\n-----Welcome to The Train Dispatch System-----");

    quit = false;

    while (!quit) {
      int menuChoice = ShowMenu.showMenu();

      switch (menuChoice) {

        case SHOW_TRAIN_DEPARTURE -> showTrainDepartures();

        case ADD_TRAIN_DEPARTURE -> addDeparture();

        case ASSIGN_TRACK -> assignTrack();

        case ADD_DELAY -> addDelay();

        case SEARCH_DEPARTURE_BY_TRAIN_NUMBER -> searchDepartureByTrainNumber();

        case SEARCH_DEPARTURES_BY_DESTINATION -> searchDeparturesByDestination();

        case CHANGE_SYSTEM_CLOCK -> changeSystemClock();

        case END_PROGRAM -> endProgram();

        default -> System.out.println("Write a number according to the menu.");

      }
    }
  }

  /**
   * Method that prints all train departures from the train station.
   */
  public void showTrainDepartures() {
    System.out.println(overview);
  }

  /**
   * Method that lets the user add a train departure.
   *
   * <p>Uses UserInputHandler-class to validate the input.
   */
  public void addDeparture() {
    try {
      System.out.print("Enter Departure Time (hh:mm): ");
      String departureTime = userInput.readStringForLocalTime();

      System.out.print("Enter Line (max 6 characters): ");
      String line = userInput.readLine();

      System.out.print("Enter a train number: ");
      int trainNumber = userInput.readPositiveInt();

      System.out.print("Enter Destination: ");
      String destination = userInput.readDestination();

      System.out.print("Enter Track (if not assigned, press Enter): ");
      String trackString = userInput.readTrackString();

      overview.addTrainDeparture(departureTime, line, trainNumber, destination, trackString);
      System.out.println("Success, departure was added.");
    } catch (InputMismatchException | IllegalArgumentException | DateTimeParseException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Method that lets the user choose a departure based on train number,
   * and write the new track for the train departure.
   */
  public void assignTrack() {
    System.out.println(overview);
    try {
      System.out.println("Write the train number for the departure you want to assign a track.");
      int trainNumber = userInput.readPositiveInt();

      System.out.println("Set track 1-6, or 0 if you want to remove assigned track.");
      int track = Integer.parseInt(userInput.readTrackString());

      overview.assignTrack(trainNumber, track);
      System.out.println("Assigned track " + track + " to departure " + trainNumber + ".");
    } catch (IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Method that lets the user choose a departure based on train number,
   * and write the new delay for the train departure.
   */
  public void addDelay() {
    System.out.println(overview);
    try {
      System.out.println("Write the train number for the departure you want to add delay to.");
      int trainNumber = userInput.readPositiveInt();

      System.out.print("Enter new delay (hh:mm): ");
      String delay = userInput.readStringForLocalTime();

      overview.addDelayToDeparture(trainNumber, delay);
      System.out.println("Added delay " + delay + " to departure " + trainNumber + ".");
    } catch (DateTimeParseException | IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Method that lets the user search for a departure using train number.
   */
  public void searchDepartureByTrainNumber() {
    System.out.println("Search for a train Number:");
    int trainNumber = userInput.readPositiveInt();

    System.out.println("Enter Destination: ");
    TrainDeparture trainDepartureByNumber = overview.getTrainDepartureByNumber(trainNumber);

    if (trainDepartureByNumber == null) {
      System.out.println("No train departure with train number " + trainNumber + " found.");
    } else {
      System.out.println(overview.headerString() + trainDepartureByNumber);
    }
  }

  /**
   * Method that lets the user search for departure(s) using destination.
   */
  public void searchDeparturesByDestination() {
    System.out.println("Search for a destination:");
    String destination = userInput.readDestination();

    List<TrainDeparture> departures = overview.getTrainDeparturesByDestination(destination);

    if (!departures.isEmpty()) {
      System.out.print(overview.headerString());
      departures.forEach(System.out::print);
    } else {
      System.out.println("No departures with destination: " + destination + " found.");
    }
  }

  /**
   * Method that lets the user change the system clock.
   */
  public void changeSystemClock() {
    try {
      System.out.println("Update clock hh:mm");
      overview.setSystemClock(userInput.readStringForLocalTime());

      System.out.println("Updated system clock to " + overview.getSystemClock());
      overview.removeTrainDepartureWithSystemClock();
    } catch (IllegalArgumentException | DateTimeParseException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * End program method.
   */
  public void endProgram() {
    System.out.println("\nExiting Train Dispatch System...");
    quit = true;
  }
}
