package edu.ntnu.stud.view;

import java.util.Scanner;

/**
 * Utility class to display a menu for the Train Departure application.
 * Presents a menu of options to user and retrieve their selected choice.
 *
 * @author 10026
 * @version 1.0
 * @since 0.5
 */
public class ShowMenu {

  /**
   * Private constructor to ensure no object of the class can be made by other objects.
   */
  private ShowMenu() {}

  /**
   * Displays a menu for the application and retrieves user menu choice.
   *
   * @return An Integer representing the user's menu choice.
   */
  public static int showMenu() {
    int menuChoice = 0;

    System.out.println("\nApplication for Train Departures");
    System.out.println("1 - Show train departures");
    System.out.println("2 - Add train departure");
    System.out.println("3 - Assign track to departure");
    System.out.println("4 - Add delay to departure");
    System.out.println("5 - Search departure by train number");
    System.out.println("6 - Search departure by destination");
    System.out.println("7 - Update system clock");
    System.out.println("9 - End program");

    Scanner scanner = new Scanner(System.in);
    if (scanner.hasNextInt()) {
      menuChoice = scanner.nextInt();
    }
    return menuChoice;
  }

}
