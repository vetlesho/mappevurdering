package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

/**
 * TrainDeparture class represents a train's departure details.
 *
 * <p>This class is responsible for the data of train's departure time, train line,
 * train number, destination, track, delay and actual departure time.
 * Provides methods to retrieve and modify details of the departure.
 *
 * <p>The class ensures input validation using its own validation methods.
 * The class also equals and toString method to enable comparison between
 * two objects and the correct display of the departure.
 *
 * <p>The class uses LocalTime for departureTime, delay and actualDeparture.
 * The class uses String for line and destination.
 * The class uses int for trainNumber and track.
 *
 * @author 10026
 * @version 1.0
 * @since 0.1
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;
  private LocalTime actualDepartureTime;

  /**
   * Validates the format of the departure time.
   * DepartureTime must be written with number in this format: hh:mm.
   *
   * @param departureTime a string representing the departure time.
   * @throws IllegalArgumentException If the provided departure time
      does not match the required format.
   */
  private void isValidTimeFormat(String departureTime) throws IllegalArgumentException {
    //Used ChatGPT to generate the regular expression.
    //Regex checks if the String is written with numbers and ':'.
    if (!departureTime.matches("\\d{2}:\\d{2}") || departureTime.isBlank()) {
      throw new IllegalArgumentException("Write departureTime like hh:mm.");
    }
  }

  /**
   * Validates the format of the train line.
   * Line must not be empty.
   *
   * @param line a String representing the train line.
   * @throws IllegalArgumentException If the provided train line is empty or blank.
   */
  private void isValidLineFormat(String line) throws IllegalArgumentException {
    if (line.isBlank() || line.length() > 6 || line.contains(" ")) {
      throw new IllegalArgumentException("Write a train line of 1-6 characters.");
    }
  }

  /**
   * Validates the train number.
   * TrainNumber must be between 1 and 9999.
   *
   * @param trainNumber a train number to be validated.
   * @throws IllegalArgumentException If the train number is not between 1 and 9999.
   */
  private void isValidTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (trainNumber <= 0 || trainNumber > 9999) {
      throw new IllegalArgumentException("Write a train number between 1 and 9999.");
    }
  }

  /**
   * Validates the format of the destination String.
   * Destination must consist of only letters.
   *
   * @param destination a String representing the destination.
   * @throws IllegalArgumentException If the destination contains non-letter characters or is blank.
   */
  private void isValidDestination(String destination) throws IllegalArgumentException {
    //Used ChatGPT to generate the regular expression.
    //Regex checks if the String only consists of norwegian letters, and is 1-20 characters long.
    if (!destination.matches("[A-Za-zæøåÆØÅ\\s]{1,20}") || destination.isBlank()) {
      throw new IllegalArgumentException("Write a destination of only letters.");
    }
  }

  /**
   * Validates the track integer.
   * Track must be between 1 and 6.
   *
   * @param track a track number to be validated.
   * @throws IllegalArgumentException If the track number is not between 1 and 6.
   */
  private void isValidTrack(int track) throws IllegalArgumentException {
    if (track < 1 || track > 6) {
      throw new IllegalArgumentException("Write a track between 1 and 6.");
    }
  }

  /**
   * Constructs a TrainDeparture object with the provided departure details.
   *
   * <p>This constructor sets LocalTime delay to 00:00 upon creation.
   * It is unlikely to create a departure that already has been delayed.
   *
   * @param departureTime Parses DepartureTime as a String to LocalTime.
   * @param line          Line/Stretch the train runs on.
   * @param trainNumber   Unique train number within 24 hours.
   * @param destination   End destination.
   * @param track         The train's assigned track.
   *
   * @throws DateTimeParseException Throws DateTimeParseException
      if the input cannot be parsed.
     (Valid values for hours: 0-23)
   */
  public TrainDeparture(String departureTime, String line, int trainNumber,
                        String destination, int track) throws DateTimeParseException {
    isValidTimeFormat(departureTime);
    isValidLineFormat(line);
    isValidTrainNumber(trainNumber);
    isValidDestination(destination);
    isValidTrack(track);

    this.departureTime = LocalTime.parse(departureTime);
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = LocalTime.of(0, 0);
    this.actualDepartureTime = LocalTime.parse(departureTime).
            plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  /**
   * Second constructor that creates a TrainDeparture object with the provided departure details.
   *
   * <p>This constructor sets LocalTime delay to 00:00 upon creation,
   * and sets the track value to -1 if it has not been assigned yet.
   *
   * @param departureTime Parses DepartureTime as a String to LocalTime.
   * @param line          Line/Stretch the train runs on.
   * @param trainNumber   Unique train number within 24 hours.
   * @param destination   End destination.
   *
   * @throws DateTimeParseException Throws DateTimeParseException
     if the input cannot be parsed.
     (Valid values for hours: 0-23)
   */
  public TrainDeparture(String departureTime, String line, int trainNumber,
                        String destination) throws DateTimeParseException {
    isValidTimeFormat(departureTime);
    isValidLineFormat(line);
    isValidTrainNumber(trainNumber);
    isValidDestination(destination);

    this.departureTime = LocalTime.parse(departureTime);
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = -1;
    this.delay = LocalTime.of(0, 0);
    this.actualDepartureTime = LocalTime.parse(departureTime).
            plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getLine() {
    return line;
  }

  public int getTrainNumber() {
    return trainNumber;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public LocalTime getDelay() {
    return delay;
  }

  public LocalTime getActualDepartureTime() {
    return actualDepartureTime;
  }

  /**
   * Set-method for track.
   *
   * <p>Checks if track is between 1 and 6.
   * If the track is 0, then it is set to -1, as described in the task.
   *
   * @param track new track from the user.
   * @throws IllegalArgumentException Throws if track has invalid values.
   */
  public void setTrack(int track) throws IllegalArgumentException {
    if (track < 0 || track > 6) {
      throw new IllegalArgumentException("Track must be between 1 and 6.");
    } else if (track == 0) {
      track = -1;
    }
    this.track = track;
  }

  /**
   * Set-method to update delay.
   *
   * <p>This calls the setActualDeparture-method to update
   * actualDeparture every time delay is updated.
   *
   * @param delay Takes in a delay String from the user hh:mm.
   * @throws DateTimeParseException Throws if the String cannot be parsed.
     (Valid values for hours: 0-23)
   */
  public void setDelay(String delay) throws DateTimeParseException {
    this.delay = LocalTime.parse(delay);
    setActualDeparture(delay);
  }

  /**
   * Set-method to update actualDeparture.
   *
   * @param delay Takes in a delay String from the user.
   * @throws DateTimeParseException Throws if the String cannot be parsed.
   */
  public void setActualDeparture(String delay) throws DateTimeParseException {
    LocalTime newTime = LocalTime.parse(delay);
    this.actualDepartureTime = departureTime
            .plusHours(newTime.getHour())
            .plusMinutes(newTime.getMinute());
  }

  /**
   * Equals method that checks if the new trainNumber already exist.
   *
   * <p>Generated by Intellij.
   *
   * @param o The object(TrainDeparture) being compared.
   * @return Returns true if the trainNumber exist and false if it doesn't exist.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainDeparture that = (TrainDeparture) o;
    return trainNumber == that.trainNumber;
  }

  /**
   * toString method that formats each trainDeparture in a specific way.
   *
   * <p>If delay equals to MIDNIGHT (00:00 = no delay), then it prints an empty slot.
   * If track value is not assigned (-1), then it prints an empty slot.
   * If actualDeparture equals departureTime (no delay), then it prints empty slot.
   *
   * @return Returns a formatted list based on the values of the TrainDeparture.
   */
  @Override
  public String toString() {
    //Used ChatGPT to create conditional statements using a ternary operator.
    //Assigns value based on the condition() being True (after ?) or False (after :).
    //This method is a precise way to write conditional statements in a simple, single line.
    //For example, if the delay equals 00:00 (i.e., no delay), then it prints an empty slot "".
    //If the delay doesn't equal 00:00, then it converses the delay to a String and the value will show.
    String delayToString = (delay.equals(LocalTime.MIDNIGHT)) ? "" : delay.toString();
    String trackToString = (track == -1) ? "" : String.valueOf(track);
    String actualDepartureToString
            = (actualDepartureTime.equals(departureTime)) ? "" : "(" + actualDepartureTime + ")";

    //String.format generated by ChatGPT and then edited by me.
    //The symbols (%-6s, %-14s, etc.) relate to each of their variables.
    //It decides how much space each variable needs to display their condition.
    String table = String.format("| %-6s %-14s | %-6s | %-12d | %-20s | %-5s | %-5s |%n",
            departureTime, actualDepartureToString, line, trainNumber,
            destination, delayToString, trackToString);

    table += "---------------------------------------------"
            + "-------------------------------------------\n";

    return table;
  }
}

