package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * TrainStation class that manages a set of train departures from a station,
 * as well as the system clock for the station.
 *
 * <p>This class primarily handles the storage, retrieval and manipulation of train departures.
 * It consists of methods to add, retrieve and remove train departures based on train number,
 * destination or departure time .
 *
 * <p>The class also provides method for the system clock.
 * This lets the user change the clock and update the system of departures.
 *
 * <p>The class uses ArrayList to store TrainDeparture objects.
 * I chose to use Arraylists because:
 *
 * <p>- It has dynamic resizing, and maintains the order
 * of the objects after for example departureTime.
 *
 * <p>- It is easy to iterate through and manipulate departures by index and methods.
 *
 * <p>- It has a straight-forward and simple approach for managing a collection.
 *
 * @author 111734
 * @version 1.0
 * @since 0.2
 */
public class TrainStation {
  private final ArrayList<TrainDeparture> trainDepartures;
  private LocalTime systemClock;

  /**
   * Constructor that creates an Arraylist of trainDepartures.
   * Gives the systemClock its initial values.
   */
  public TrainStation() {
    this.trainDepartures = new ArrayList<>();
    this.systemClock = LocalTime.of(9, 0);
  }

  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  public LocalTime getSystemClock() {
    return systemClock;
  }

  /**
   * Method for the user to set the SystemClock.
   *
   * <p>The method takes in a String and tries to parse to LocalTime.
   * It is not possible to set the clock to an earlier time.
   *
   * @param newSystemClockString Takes in a String "hh:mm" from the user.
   * @throws DateTimeParseException if the input cannot be parsed.
   * @throws IllegalArgumentException if the newSystemClock is before current time.
   */
  public void setSystemClock(String newSystemClockString)
          throws DateTimeParseException, IllegalArgumentException {
    LocalTime newSystemClock = LocalTime.parse(newSystemClockString);
    if (newSystemClock.isBefore(systemClock)) {
      throw new IllegalArgumentException("Cannot set clock back in time.");
    }
    systemClock = newSystemClock;
  }

  /**
   * Method that adds a trainDeparture to the lists of trainDepartures.
   *
   * <p>First the method checks if the user has written an empty trackString,
   * to decide which constructor in TrainDeparture to use.
   * The method uses equals method from the TrainDeparture
   * class to check if the trainNumber already exists.
   *
   * @param departureTime DepartureTime (String) to the train.
   * @param line Line (String) the train runs on.
   * @param trainNumber Unique train number (int) within 24 hours.
   * @param destination End destination (String).
   * @param trackString Parses trackString (String) to an Integer.
   * @throws IllegalArgumentException if a trainDeparture with same trainNumber already exist.
   */
  public void addTrainDeparture(String departureTime, String line, int trainNumber,
                                String destination, String trackString)
          throws IllegalArgumentException {

    TrainDeparture newDeparture;
    if (trackString.isEmpty()) {
      newDeparture = new TrainDeparture(departureTime, line, trainNumber, destination);
    } else {
      int track = Integer.parseInt(trackString);
      newDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track);
    }

    if (trainDepartures.contains(newDeparture)) {
      throw new IllegalArgumentException("Train departure with train number: "
              + trainNumber + " already exist.");
    } else {
      trainDepartures.add(newDeparture);
      sortTrainDeparturesByDepartureTime();
      removeTrainDepartureWithSystemClock();
    }
  }

  /**
   * Method that sorts trainDepartures by actualDeparture using Comparator.
   */
  public void sortTrainDeparturesByDepartureTime() {
    trainDepartures.sort(Comparator.comparing(TrainDeparture::getActualDepartureTime));
  }

  /**
   * Method that returns a TrainDeparture with the given trainNumber.
   *
   * <p>Creates a stream from trainDepartures,
   * then filters the stream using a lambda expression,
   * checking for the first element with the same trainNumber.
   * If no element matches the trainNumber, then it returns null.
   *
   * @param trainNumber Takes in a trainNumber from the user.
   * @return A TrainDeparture with the given trainNumber, or null.
   */
  public TrainDeparture getTrainDepartureByNumber(int trainNumber) {
    return trainDepartures.stream()
            .filter(td -> td.getTrainNumber() == trainNumber)
            .findFirst()
            .orElse(null);
  }

  /**
   * Method that returns a list of every TrainDeparture with the given destination.
   *
   * <p>Creates a stream from trainDepartures,
   * then filters the stream using a lambda expression,
   * checking for departures with the same destination.
   * Collects the elements to a list.
   *
   * @param destination takes in a destination from the user.
   * @return A list of departures with the given destination.
   */
  public List<TrainDeparture> getTrainDeparturesByDestination(String destination) {
    return trainDepartures.stream()
            .filter(td -> destination.equalsIgnoreCase(td.getDestination()))
            .collect(Collectors.toList());
  }

  /**
   * Method that assigns a track to given TrainDeparture.
   *
   * <p>Uses getTrainDepartureByNumber method to return a departure with exact trainNumber.
   *
   * @param trainNumber Takes in a train number Integer from the user.
   * @param track Takes in a track Integer from the user.
   * @throws IllegalArgumentException if the train departure doesn't exist.
   */
  public void assignTrack(int trainNumber, int track) throws IllegalArgumentException {
    TrainDeparture departure = getTrainDepartureByNumber(trainNumber);
    if (departure == null) {
      throw new IllegalArgumentException("Train number: " + trainNumber + " not found.");
    }
    departure.setTrack(track);
  }

  /**
   * Method that adds delay to chosen departure.
   *
   * @param trainNumber Takes in a train number Integer from the user.
   * @param delay Takes in a delay String from the user.
   * @throws IllegalArgumentException if the trainNumber is not found.
   */
  public void addDelayToDeparture(int trainNumber, String delay) throws IllegalArgumentException {
    TrainDeparture departure = getTrainDepartureByNumber(trainNumber);
    if (departure == null) {
      throw new IllegalArgumentException("Train number: " + trainNumber + " not found.");
    }
    departure.setDelay(delay);
    sortTrainDeparturesByDepartureTime();
  }

  /**
   * Method that removes every trainDeparture if it's
   * actual departure time is before the systemClock.
   *
   * <p>Goes through every element and removes departures using a
   * lambda expression and a removeIf-statement.
   */
  public void removeTrainDepartureWithSystemClock() {
    trainDepartures.removeIf(trainDeparture ->
            systemClock.isAfter(trainDeparture.getActualDepartureTime()));
  }

  /**
   * Generates a header for the table.
   *
   * <p>This method could arguably be in the UI class,
   * because it's main purpose is to print a clear header.
   * But since it uses the systemClock variable and
   * is used every time the TrainStation data is retrieved,
   * I chose to have it in the TrainStation class.
   * This makes it possible to call this method in the UI-class,
   * to print the header with a set of departures, instead of the whole list.
   *
   * @return a String of the table header.
   */
  public String headerString() {
    //Used ChatGPT to generate the header.
    //I have edited it, so it matches the table in TrainDeparture.
    //I added the Current time and systemClock to the header.
    String table = "------------------------------------------"
            + "----------------------------------------------\n";
    table += "| Current time                           "
            + "                                        "
            + systemClock +  " |\n";
    table += "---------------------------------------------"
            + "-------------------------------------------\n";
    table += "| Scheduled (Expected)  | Line   | Train Number "
            + "| Destination          | Delay | Track |\n";
    table += "---------------------------------------------"
            + "-------------------------------------------\n";
    return table;
  }

  /**
   * ToString that displays every TrainDeparture.
   *
   * <p>Creates a stream from the trainDepartures list,
   * converts every object (TrainDeparture) to a String,
   * and then collects all the strings (of every TrainDeparture) to a single String.
   *
   * @return the table header and a String of departures.
   */
  public String toString() {
    String trainDeparturesString = trainDepartures.stream()
            .map(TrainDeparture::toString)
            .collect(Collectors.joining());
    return headerString() + trainDeparturesString;
  }
}
