package edu.ntnu.stud;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainStation;
import org.junit.jupiter.api.*;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the TrainStation class.
 *
 *<p>The class test methods for the systemClock, and methods for the TrainStation.
 * It checks for expected and unexpected errors.
 *
 *<p>It does not test the headerString method or toString method.
 */

class TrainStationTest {
  private TrainStation testOverview;
  private final String DEPARTURE_TIME =  "20:00";
  private final String LINE = "L4";
  private final int TRAIN_NUMBER = 12;
  private final String DESTINATION = "Bergen";
  private final String TRACK_STRING = "6";

  @BeforeEach
  void setUp() {
    testOverview = new TrainStation();
    testOverview.addTrainDeparture("14:00", "A", 1, "Oslo","1");
    testOverview.addTrainDeparture("13:00", "B", 2, "Bergen","2");
  }

  @Nested
  @DisplayName("Tests for systemClock")
  class testSystemClock {
    @Test
    @DisplayName("Get system clock returns expected time")
    void getSystemClock() {
      assertEquals(LocalTime.of(9,0), testOverview.getSystemClock());
      assertNotEquals(LocalTime.of(10,0), testOverview.getSystemClock());
    }

    @Test
    @DisplayName("Set system clock with valid input")
    void setSystemClock() {
      String newTime = "22:00";
      assertDoesNotThrow(() -> testOverview.setSystemClock(newTime));
      assertEquals(LocalTime.parse(newTime), testOverview.getSystemClock());
    }

    @Test
    @DisplayName("Set system clock with invalid input should throw DateTimeParseException")
    void setSystemClock_InvalidInput() {
      assertThrows(DateTimeParseException.class, () -> testOverview.setSystemClock("invalidTime"));
    }
    @Test
    @DisplayName("Set system clock with parse exceptions should throw DateTimeParseException")
    void setSystemClock_ParseExceptions() {
      assertThrows(DateTimeParseException.class, () -> testOverview.setSystemClock("25:00"));
    }

    @Test
    @DisplayName("Set system clock cannot go back in time")
    void setSystemClock_CannotGoBackInTime() {
      assertThrows(IllegalArgumentException.class, () -> testOverview.setSystemClock("01:00"));
    }
  }

  @Nested
  @DisplayName("Tests for addDeparture")
  class testAddDeparture {
    @Test
    @DisplayName("Add train departure to list (non-empty track)")
    void addTrainDepartureToListShouldWork() {
      testOverview.addTrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER, DESTINATION, TRACK_STRING);

      assertEquals(3, testOverview.getTrainDepartures().size());
      assertEquals(TRAIN_NUMBER, testOverview.getTrainDepartures().get(2).getTrainNumber());
    }
    @Test
    @DisplayName("Add train departure to list (empty track)")
    void addTrainDepartureToListShouldWorkWithEmptyTrack() {
      String emptyTrack = "";
      testOverview.addTrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER, DESTINATION, emptyTrack);

      assertEquals(3, testOverview.getTrainDepartures().size());
      assertEquals(TRAIN_NUMBER, testOverview.getTrainDepartures().get(2).getTrainNumber());
    }

    @Test
    @DisplayName("Add train departure should not work if duplicate")
    void addTrainDepartureShouldNotWorkDuplicate() {
      testOverview.addTrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER, DESTINATION, TRACK_STRING);

      assertThrows(IllegalArgumentException.class, () ->
              testOverview.addTrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER, DESTINATION, TRACK_STRING));
    }
  }

  @Nested
  @DisplayName("Tests for streams methods")
  class streamMethods {
    @Test
    @DisplayName("Sort train departures by departure time")
    void testSortTrainDeparturesByDepartureTime() {
      testOverview.sortTrainDeparturesByDepartureTime();
      assertEquals(LocalTime.parse("13:00"), testOverview.getTrainDepartures().get(0).getActualDepartureTime());
      assertEquals(LocalTime.parse("14:00"), testOverview.getTrainDepartures().get(1).getActualDepartureTime());
    }

    @Test
    @DisplayName("Get train departure by existing number")
    void getTrainDepartureByExistingNumber() {
      TrainDeparture foundDeparture = testOverview.getTrainDepartureByNumber(2);

      assertEquals(2, foundDeparture.getTrainNumber());
    }

    @Test
    @DisplayName("Get train departure by non-existing number")
    void getTrainDepartureByNonExistingNumber() {
      assertNull(testOverview.getTrainDepartureByNumber(999));
    }

    @Test
    @DisplayName("Get train departures by valid destination")
    void getTrainDeparturesByDestination() {
      List<TrainDeparture> foundDepartures = testOverview.getTrainDeparturesByDestination("Bergen");

      assertEquals(1, foundDepartures.size());
      assertEquals("Bergen", foundDepartures.get(0).getDestination());
    }

    @Test
    @DisplayName("Get train departures by non-existing destination")
    void getTrainDeparturesByNoDestination() {
      List<TrainDeparture> foundDepartures = testOverview.getTrainDeparturesByDestination("Trondheim");

      assertEquals(0, foundDepartures.size());
    }

    @Test
    @DisplayName("Remove train departures that is before the system clock.")
    void removeTrainDepartureWithSystemClock() {
      testOverview.setSystemClock("13:30");
      testOverview.removeTrainDepartureWithSystemClock();

      assertNull(testOverview.getTrainDepartureByNumber(2));
      assertNotNull(testOverview.getTrainDepartureByNumber(1));
    }
  }

  @Nested
  @DisplayName("Tests for assign track")
  class assignTrackTesting {
    @Test
    @DisplayName("Assign track to existing train departure")
    void testAssignTrackToExistingTrainDeparture() {
      int trackToAssign = 3;
      testOverview.assignTrack(1, trackToAssign);

      TrainDeparture assignedDeparture = testOverview.getTrainDepartureByNumber(1);
      assertEquals(trackToAssign, assignedDeparture.getTrack());
    }

    @Test
    @DisplayName("Assign track to non-existent train departure")
    void testAssignTrackToNonExistentTrainDeparture() {
      int NoneExistentTrainNumber = 5;

      assertThrows(IllegalArgumentException.class, () -> testOverview.assignTrack(NoneExistentTrainNumber, 2));
    }

    @Test
    @DisplayName("Assign invalid track to existing train departure")
    void testAssignInvalidTrackToExistingTrainDeparture() {
      int invalidTrack = -1;

      assertThrows(IllegalArgumentException.class, () -> testOverview.assignTrack(1, invalidTrack));
    }
  }

  @Nested
  @DisplayName("Tests for add delay")
  class addDelayTest {
    @Test
    @DisplayName("Add valid delay to existing departure")
    void testAddValidDelayToExistingDeparture() {
      String delayToAdd = "00:10";
      String actualDeparture = "14:10";
      testOverview.addDelayToDeparture(1, delayToAdd);

      TrainDeparture delayedDeparture = testOverview.getTrainDepartureByNumber(1);
      assertEquals(LocalTime.parse(delayToAdd), delayedDeparture.getDelay());
      assertEquals(LocalTime.parse(actualDeparture), delayedDeparture.getActualDepartureTime());
    }
    @Test
    @DisplayName("Add valid delay to non-existent train departure")
    void testAddValidDelayToNonexistentTrainDeparture() {
      int NoneExistentTrainNumber = 1000;
      String delayToAdd = "00:10";

      assertThrows(IllegalArgumentException.class, () ->
              testOverview.addDelayToDeparture(NoneExistentTrainNumber, delayToAdd));

    }

    @Test
    @DisplayName("Add invalid delay to existing train departure")
    void testAddInvalidDelayToExistingTrainDeparture() {
      int invalidTrainNumber = 1;
      String delayToAdd = "25:00";

      assertThrows(DateTimeParseException.class, () ->
              testOverview.addDelayToDeparture(invalidTrainNumber, delayToAdd));
    }
  }

  @AfterEach
  void tearDown() {
    testOverview = null;
  }
}