package edu.ntnu.stud;

import edu.ntnu.stud.model.TrainDeparture;
import org.junit.jupiter.api.*;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for TrainDeparture.
 *
 * <p>This class tests constructors, getters, setters, validation and equals methods.
 *
 * <p>It does not test the toString method.
 */
class TrainDepartureTest {
  private TrainDeparture test1;
  private TrainDeparture test2;

  @BeforeEach
  void setUp() {
    test1 = new TrainDeparture("14:00", "A", 1, "Oslo",1);
    test2 = new TrainDeparture("13:00", "B", 2, "Bergen",2);
  }

  @Nested
  @DisplayName("Validation tests for the exceptions.")
  class ValidationTests {
    @Test
    @DisplayName("Valid time format should not throw exception")
    void validTimeFormatShouldNotThrowException() {
      assertDoesNotThrow(() -> new TrainDeparture("14:00", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Invalid time format should throw IllegalArgumentException")
    void invalidTimeFormatShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("willFail", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Invalid time format should throw DateTimeParseException")
    void invalidTimeFormatShouldThrowDateTimeParseException() {
      assertThrows(DateTimeParseException.class, () -> new TrainDeparture("25:20", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Valid line format should not throw exception")
    void validLineFormatShouldNotThrowException() {
      assertDoesNotThrow(() -> new TrainDeparture("14:00", "Lin3", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Empty line should throw IllegalArgumentException")
    void emptyLineShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Long line should throw IllegalArgumentException")
    void longLineShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "ToLongLine", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Line with spaces should throw IllegalArgumentException")
    void lineWithSpacesShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Spa ce", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Valid train number should not throw exception")
    void validTrainNumberShouldNotThrowException() {
      assertDoesNotThrow(() -> new TrainDeparture("14:00", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Train number exceeding range should throw IllegalArgumentException")
    void trainNumberExceedingRangeShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 10000, "Destination", 1));
    }

    @Test
    @DisplayName("Negative train number should throw IllegalArgumentException")
    void negativeTrainNumberShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", -1, "Destination", 1));
    }

    @Test
    @DisplayName("Valid destination format should not throw exception")
    void validDestinationFormatShouldNotThrowException() {
      assertDoesNotThrow(() -> new TrainDeparture("14:00", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Empty destination should throw IllegalArgumentException")
    void emptyDestinationShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 3, "", 1));
    }

    @Test
    @DisplayName("Long destination should throw IllegalArgumentException")
    void longDestinationShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 3, "WayToLoooooooooongDestination", 1));
    }

    @Test
    @DisplayName("Destination with numbers should throw IllegalArgumentException")
    void destinationWithNumbersShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 3, "Numbers123", 1));
    }

    @Test
    @DisplayName("Valid track should not throw exception")
    void validTrackShouldNotThrowException() {
      assertDoesNotThrow(() -> new TrainDeparture("14:00", "Line", 3, "Destination", 1));
    }

    @Test
    @DisplayName("Track exceeding range should throw IllegalArgumentException")
    void trackExceedingRangeShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 3, "Destination", 10));
    }

    @Test
    @DisplayName("Negative track should throw IllegalArgumentException")
    void negativeTrackShouldThrowIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("12:00", "Line", 3, "Destination", -2));
    }
  }



  @Nested
  @DisplayName("Getters for TrainDeparture")
  class gettersTrainDeparture{
    @Test
    void getDepartureTime() {
      assertEquals(LocalTime.parse("14:00"), test1.getDepartureTime());
    }

    @Test
    void getLine() {
      assertEquals("A", test1.getLine());
    }

    @Test
    void getTrainNumber() {
      assertEquals(1, test1.getTrainNumber());
    }

    @Test
    void getDestination() {
      assertEquals("Oslo", test1.getDestination());
    }

    @Test
    void getTrack() {
      assertEquals(1, test1.getTrack());
    }

    @Test
    void getDelay() {
      LocalTime delay = LocalTime.of(0,0);
      assertEquals(delay, test2.getDelay());
    }

    @Test
    void getActualDeparture() {
      LocalTime expectedActualDeparture = LocalTime.parse("14:00");
      assertEquals(expectedActualDeparture, test1.getActualDepartureTime());
    }
  }

  @Nested
  @DisplayName("Testers for setDelay")
  class setDelayTests {
    @Test
    @DisplayName("Test setDelay with valid input")
    void setDelayValid() {
      String delay = "00:05";
      assertDoesNotThrow(() -> test1.setDelay(delay));
      assertEquals(LocalTime.parse(delay), test1.getDelay());
    }
    @Test
    @DisplayName("Test setDelay with invalid input")
    void setDelayInvalidInput() {
      String delay = "testFail";
      assertThrows(DateTimeParseException.class, () -> test1.setDelay(delay));
    }
    @Test
    @DisplayName("Test setDelay with invalid LocalTime(ParseException)")
    void setDelayInvalidLocalTime() {
      String delay = "25:99";
      assertThrows(DateTimeParseException.class, () -> test1.setDelay(delay));
    }
  }
  @Nested
  @DisplayName("Testers for setActualDeparture")
  class setActualDepartureTests {
    @Test
    @DisplayName("Test setActualDeparture valid input")
    void setActualDepartureValid() {
      String delay = "00:10";
      LocalTime expectedActualDeparture = LocalTime.parse("14:10");

      assertDoesNotThrow(() -> test1.setActualDeparture(delay));
      assertEquals(expectedActualDeparture, test1.getActualDepartureTime());
    }
    @Test
    @DisplayName("Test setActualDeparture with invalid input")
    void setActualDepartureInvalidInput() {
      String delay = "willFail";

      assertThrows(DateTimeParseException.class, () -> test1.setActualDeparture(delay));
    }
    @Test
    @DisplayName("Test setActualDeparture with invalid LocalTime(ParseException)")
    void setActualDepartureInvalidLocalTime() {
      String delay = "99:99";

      assertThrows(DateTimeParseException.class, () -> test1.setActualDeparture(delay));
    }
  }

  @Nested
  @DisplayName("Testers for setTrack")
  class setTrackTests {
    @Test
    @DisplayName("test setTrack with valid track 0")
    void setTrackTo0Valid() {
      int track = 0;
      assertDoesNotThrow(() -> test1.setTrack(track));
      assertEquals(-1, test1.getTrack());
    }
    @Test
    @DisplayName("test setTrack with valid track 1-6")
    void setTrackValid() {
      int track = 4;
      assertDoesNotThrow(() -> test1.setTrack(track));
      assertEquals(track, test1.getTrack());
    }
    @Test
    @DisplayName("test setTrack with invalid too low")
    void setTrackInvalidTooLow() {
      int track = -5;
      assertThrows(IllegalArgumentException.class, () -> test1.setTrack(track));
    }
    @Test
    @DisplayName("test setTrack with invalid too high")
    void setTrackInvalidTooHigh() {
      int track = 55;
      assertThrows(IllegalArgumentException.class, () -> test1.setTrack(track));
    }
  }

  @Nested
  @DisplayName("Equal and not Equal")
  class equalAndNotEqual {
    @Test
    @DisplayName("Test equals when object with same trainNumber exist")
    void testEqualsWhenEqual() {
      TrainDeparture test3 = new TrainDeparture("18:00", "C",
              1, "Trondheim", 3);
      assertEquals(test1, test3);
    }

    @Test
    @DisplayName("Test equals when object with same trainNumber dont exist")
    void testEqualsWhenNotEqual() {
      assertNotEquals(test1, test2);
    }
    @Test
    @DisplayName("Test equals when object with null object")
    void testEqualsWithNullObject() {
      assertNotEquals(null, test1);
    }
  }

  @AfterEach
  void tearDown() {
    test1 = null;
    test2 = null;
  }
}