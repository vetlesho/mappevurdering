# Portfolio project IDATA1003 - 2023

STUDENT NAME = "Vetle Solheim Hodne"  

STUDENT ID = "111734"

## Project description

This project is called "Train Dispatch System", and presents train departures from a train station. 
<p>The system lets users manage and edit the train departures through a menu.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
I have used packages in my files to structure my project. 
<p>In the launcher-folder, I have TrainDispatchApp that starts the program.
<p>The model-folder holds classes representing the database, TrainDeparture and TrainStation. 
<p>Lastly, the view-folder holds UI-related files, the UserInterface-, ShowMenu- and UserInputHandler-classes. 
<p>My tests are stored in the test folder. To run the tests, read "How to run the tests".

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/vetlesho/mappevurdering/-/tree/master

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
The program runs from the TrainDispatchApp-class. 
<p>Go to mappevurdering -> src -> main -> java -> edu.ntnu.stud -> launcher -> TrainDispatchApp and press run.
<p>The main method is in this class and starts the application. 

<p>The system lets the user manage the system, either by creating, updating or searching for the departures.
It is also possible to update the system clock, that represents a real life clock for the program.
<p>The output of the program is a menu, and shows methods to print, add, edit departures, etc. 
The expected behavior is that the user follows the instructions given in the output.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
To run the tests, go to mappevurdering->src->test->java->edu.ntnu.stud.
<p>When the user is in one of the test classes, he or she can run the tests.

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
Horstmann, C. S. (2021). Core Java, Volume I: Fundamentals (12. utgave). Oracle Press.
<p>https://www.geeksforgeeks.org/java/?ref=shm_outind 
<p>https://www.w3schools.com 
<p>https://stackoverflow.com 
<p>https://chat.openai.com 
